# GGTracer iOS 集成



## 下载并导入SDK

1. **手动导入**

   从[下载中心]()下载 iOS 最新版本 SDK
   解压 SDK 压缩包，将文件夹导入工程中

2. cocoapods

   ```objc
   pod 'GGTracer'
   ```


## 初始化

在AppDelegate中，增加头文件的引用：

```objc
#import "GGTracer.h"
```

在`application: didFinishLaunchingWithOptions`方法中调用`initWithDelegate`方法来初始化 SDK，如下代码所示：

```objc
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [GGTracerManager initWithAppKey:@"appkey" delegate:self];
    return YES;
}
```



## 使用

在用户注册之前调用下面的方法。可以获取到渠道号和邀请码。

```objc
[[GGTracerManager defaultManager] getInstallParmsCompleted:^(GGTracerModel * _Nullable model) {
     // 渠道号
     NSString *channelCode = model.channelCode;
     // 其它信息
     NSDictionary *dataDic = model.data;   
}];
```

注册量统计。用户注册成功后调用

```objc
[GGTracerManager reportRegister];
```



## 从网页端唤醒

从网页端唤醒使用通用链接(Universal Link)，只适用于iOS9以上。

设置 Targets->Capabilities->Associated Domains

添加关联的域名，如``` applinks:www.baidu.com```

在AppDelegate中进行注册

```objc
- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray<id<UIUserActivityRestoring>> * _Nullable))restorationHandler {
    
    BOOL isOK = [GGTracerManager continueUserActivity:userActivity];
    if (isOK) {
        return YES;
    }
    
    return YES;
}
```

实现```GGTracerDelegate```的代理方法

```objc
- (void)getWakeUpParams:(GGTracerModel *)model {
    // 从H5跳转到应用传过来的参数
    NSDictionary *dataDic = model.data;
    NSLog(@"%@", model);
}
```

