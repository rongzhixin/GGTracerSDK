//
//  GGTracerManager.h
//  GGTracer
//
//  Created by 飞鱼 on 2018/12/6.
//  Copyright © 2018 RongZhiXin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GGTracerModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol GGTracerDelegate<NSObject>

@optional

/**
 App从H5唤醒

 @param model H5 页面传递过来的参数
 */
- (void)getWakeUpParams:(nullable GGTracerModel *)model;

@end

@interface GGTracerManager : NSObject

/**
 GGTracerManager单例
 不要用其它方式创建
 @return GGTracerManager
 */
+ (instancetype)defaultManager;

/**
 初始化SDK
 
 @param appKey appKey
 @param delegate 代理
 */
+ (void)initWithAppKey:(NSString *)appKey delegate:(id _Nonnull)delegate;

/**
 获取用户安装app后由web网页传递过来的”动态参数“（如邀请码、游戏房间号，渠道编号等)
 默认回调超时时间为5秒(s),不满足需求可用下面的方法
 @param completedBlock 回调block
 */
- (void)getInstallParmsCompleted:(void (^_Nullable)(GGTracerModel*_Nullable model))completedBlock;

/**
 获取用户安装app后由web网页传递过来的”动态参数“（如邀请码、游戏房间号，渠道编号等)
 
 @param timeoutInterval 回调超时时间
 @param completedBlock 回调block
 */
- (void)getInstallParmsWithTimeoutInterval:(NSTimeInterval)timeoutInterval
                                 completed:(void (^_Nullable)(GGTracerModel*_Nullable model))completedBlock;

/**
 通过网址打开应用。
 在AppDelegate - continueUserActivity 方法中注册，通过GGTracerDelegate代理处理
 本质是通用链接(Universal Link)。
 
 @param userActivity 存储了页面信息，包括url
 @return URL是否被OpenInstall识别
 */
+ (BOOL)continueUserActivity:(NSUserActivity*_Nullable)userActivity;

/**
 注册量统计，用户注册成功后调用
 */
+ (void)reportRegister;

@end

NS_ASSUME_NONNULL_END
