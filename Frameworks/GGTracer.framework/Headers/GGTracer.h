//
//  GGTracer.h
//  GGTracer
//
//  Created by 飞鱼 on 2018/12/6.
//  Copyright © 2018 RongZhiXin. All rights reserved.
//

#import <Foundation/Foundation.h>

#if __has_include(<GGTracer/GGTracer.h>)

FOUNDATION_EXPORT double GGTracerVersionNumber;
FOUNDATION_EXPORT const unsigned char GGTracerVersionString[];

#import <GGTracer/GGTracerManager.h>

#else

#import "GGTracerManager.h"

#endif

