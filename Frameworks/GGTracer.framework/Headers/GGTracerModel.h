//
//  GGTracerModel.h
//  GGTracer
//
//  Created by 飞鱼 on 2018/12/6.
//  Copyright © 2018 RongZhiXin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GGTracerModel : NSObject<NSCopying>

- (instancetype)initWithData:(NSDictionary *)data
                 channelCode:(NSString *)channelCode;


@property (nonatomic,strong) NSDictionary *data;//动态参数
@property (nonatomic,copy) NSString *channelCode;//渠道编号

@end

NS_ASSUME_NONNULL_END
