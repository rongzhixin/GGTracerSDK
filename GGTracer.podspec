Pod::Spec.new do |spec|
  spec.name         = "GGTracer"
  spec.version      = "1.0.0"
  spec.summary      = "GGTracer"
  spec.description  = <<-DESC
  GGTracer GGTracer GGTracer
                   DESC
  spec.homepage     = "https://gitee.com/rongzhixin/GGTracerSDK"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author             = { "飞鱼" => "870027381@qq.com" }
  spec.platform     = :ios, "9.0"
  spec.source       = { :git => "https://gitee.com/rongzhixin/GGTracerSDK.git", :tag => "#{spec.version}" }
  spec.vendored_frameworks = 'Frameworks/*.framework'

end